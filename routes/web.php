<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/contact.html', [App\Http\Controllers\HomeController::class, 'contact'])->name('contact');
Route::get('/about.html', [App\Http\Controllers\HomeController::class, 'about'])->name('about');
Route::get('/post.html', [App\Http\Controllers\HomeController::class, 'post'])->name('post');
Route::get('/index.html', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



